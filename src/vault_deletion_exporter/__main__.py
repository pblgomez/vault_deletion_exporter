#!/usr/bin/env python

from datetime import datetime, timedelta
from os import environ, path
from time import sleep

from hvac import Client as hvc
from prometheus_client import Gauge, start_http_server

root_path = "/"
VAULT_ADDR = environ["VAULT_ADDR"]
VAULT_TOKEN = environ["VAULT_TOKEN"]
VAULT_MNTPOINT = environ["VAULT_MNTPOINT"]

client = hvc(url=VAULT_ADDR, token=VAULT_TOKEN)


def get_deletion_time(secret):
    metadata = client.kv.v2.read_secret_metadata(mount_point=VAULT_MNTPOINT, path=secret)
    version = client.kv.v2.read_secret_version(mount_point=VAULT_MNTPOINT, path=secret, raise_on_deleted_version=False)["data"]["metadata"]["version"]
    deletion_time = metadata["data"]["versions"][f"{version}"]["deletion_time"]
    if deletion_time:
        deletion_time = datetime.fromisoformat(deletion_time[:-4])
        return deletion_time


def extract_files(extracted_secrets, internal_path):
    secrets = client.secrets.kv.v2.list_secrets(mount_point=VAULT_MNTPOINT, path=internal_path)
    for secret in secrets["data"]["keys"]:
        if secret.endswith("/"):
            new_path = path.join(internal_path, secret)
            extracted_secrets = extract_files(extracted_secrets, new_path)
        else:
            extracted_secrets.append(internal_path + secret)
    return extracted_secrets


def main():
    start_http_server(8000)
    g = Gauge("about_to_expire_less_15_days", "Date of expiration of the secret", ("secret",))
    while True:
        extracted_secrets = []
        extracted_secrets = extract_files(extracted_secrets, root_path)
        for secret in extracted_secrets:
            deletion_time = get_deletion_time(secret)
            if deletion_time:
                about_to_expire = deletion_time - datetime.utcnow() < timedelta(minutes=15)
                if about_to_expire:
                    print(f"Secret {secret} is about to expire on {deletion_time}")
                    timestamp = deletion_time.timestamp()
                    g.labels(secret).set(timestamp)
            else:
                pass
        sleep(15)


if __name__ == "__main__":
    main()
