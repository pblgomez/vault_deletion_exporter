ARG PYTHON="3.11"

FROM python:$PYTHON-slim as python-deps
RUN pip install --upgrade pip \
        && pip install poetry==1.4.0
WORKDIR /app
RUN poetry config --local virtualenvs.in-project true
COPY pyproject.toml poetry.lock .
RUN poetry install --only main --no-root --no-interaction -vvv


FROM python:$PYTHON-alpine as runtime
WORKDIR /app
COPY --from=python-deps /app /app
COPY src/*/* ./
ENV PATH="/app/.venv/bin:$PATH"
EXPOSE 8000
ENTRYPOINT ["python", "/app"]
