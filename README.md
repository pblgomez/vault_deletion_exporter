# Vault Deletion exporter

## Description

A Hashicorp Vault prometheus exporter of static secrets with `deletion_time` with less than 15 days

## Usage

You need to export envs:

- VAULT_ADDR
- VAULT_TOKEN
- VAULT_MNTPOINT

And it will expose in port 8000
